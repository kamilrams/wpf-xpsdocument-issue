﻿namespace XpsDocumentImageTest
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Markup;
    using System.Windows.Media.Imaging;

    internal class FlowDocumentBuilder
    {
        public FlowDocument Build()
        {
            var document = new FlowDocument();

            var paragraph = new Paragraph();
            var text = new TextBlock() { Text = "Sample text" };
            var image = this.CreateImage();

            this.AddChildToElement(text, paragraph);
            this.AddChildToElement(image, paragraph);
            this.AddChildToElement(paragraph, document);

            return document;
        }

        private Image CreateImage()
        {
            var uriSource = "pack://application:,,,/XpsDocumentImageTest;component/Resources/sample.gif";
            var uri = new Uri(uriSource);
            var img = new BitmapImage(uri);

            return new Image()
            {
                Source = img,
                UseLayoutRounding = true,
            };
        }

        private void AddChildToElement(object child, object targetElement)
        {
            if (targetElement is IAddChild parent)
            {
                parent.AddChild(child);
            }
        }
    }
}