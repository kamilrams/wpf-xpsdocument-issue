﻿namespace XpsDocumentImageTest
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Documents;

    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        private FixedDocumentSequence document;

        public MainWindowViewModel()
        {
            var builder = new FlowDocumentBuilder();
            var converter = new FlowToXpsDocumentConverter();

            var flowDocument = builder.Build();
            var xpsDocument = converter.Convert(flowDocument);

            this.Document = xpsDocument.GetFixedDocumentSequence();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public FixedDocumentSequence Document
        {
            get => this.document;
            set
            {
                if (this.document != value)
                {
                    this.document = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            var args = new PropertyChangedEventArgs(propertyName);

            this.PropertyChanged?.Invoke(this, args);
        }
    }
}