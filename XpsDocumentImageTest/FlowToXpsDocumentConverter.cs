﻿namespace XpsDocumentImageTest
{
    using System;
    using System.IO;
    using System.IO.Packaging;
    using System.Printing;
    using System.Windows.Documents;
    using System.Windows.Xps.Packaging;

    internal class FlowToXpsDocumentConverter
    {
        private const string UriPattern = @"memorystream://{0}";
        private const CompressionOption XpsCompressionOption = CompressionOption.NotCompressed;

        public XpsDocument Convert(FlowDocument document)
        {
            return this.PerformConversion(document);
        }

        private XpsDocument PerformConversion(IDocumentPaginatorSource sourceDocument)
        {
            var stream = new MemoryStream();
            var uri = this.CreateUniqueUri();
            var package = Package.Open(stream, FileMode.OpenOrCreate);

            PackageStore.AddPackage(uri, package);

            var xpsDocument = new XpsDocument(package, XpsCompressionOption)
            {
                Uri = uri,
            };

            var xpsWriter = XpsDocument.CreateXpsDocumentWriter(xpsDocument);
            var printTicket = this.ProvidePrintTicket();

            xpsWriter.Write(sourceDocument.DocumentPaginator, printTicket);

            //package.Close();

            //var copiedDoc = this.CopyDocumentFromStream(stream);

            return xpsDocument;
        }

        private XpsDocument CopyDocumentFromStream(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);

            var package = Package.Open(stream, FileMode.Open, FileAccess.Read);
            var uri = this.CreateUniqueUri();

            PackageStore.AddPackage(uri, package);

            var xpsDocument = new XpsDocument(package, XpsCompressionOption)
            {
                Uri = uri,
            };

            return xpsDocument;
        }

        private Uri CreateUniqueUri()
        {
            var guid = Guid.NewGuid()
                           .ToString("N");

            var uniqueUriPath = string.Format(UriPattern, guid);

            return new Uri(uniqueUriPath, UriKind.Absolute);
        }

        private PrintTicket ProvidePrintTicket()
        {
            var sizeA4 = new PageMediaSize(PageMediaSizeName.ISOA4);

            return new PrintTicket()
            {
                PageMediaSize = sizeA4,
            };
        }
    }
}